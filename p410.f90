        Program P410
        Dimension Z(40),P(40)
        Double Precision k,kp,KK,KKp,U,Sn,Z,P,Pi,Fp,Fs
        Pi=3.1415926535D0
        Write(*,*) 'Input N, Fp and Fs'
        Read(*,*) N,Fp,Fs
        k=Fp/Fs
        Call Landen(k,KK)
        Write(*,53) " k=",k," K(k)=",KK
53      Format (2X,A,F16.10,6X,A,F16.10)
        kp=DSqrt(1-k*k)
        Call Landen(kp,KKp)
        Write(*,55) "k'=",kp,"K(k')=",KKp
55      Format (2X,A,F16.10,6X,A,F16.10)
        Do 12 I=1,N/2
        U=(2*I)*KK/N
        IF(N.EQ.2*(N/2)) U=(2*I-1)*KK/N
        Call SSN(U,k,KK,KKp,Sn)
        Z(I)=SN
        P(I)=1/(k*SN)
        Write(*,20) I,Z(I),I,P(I)
20      Format (1X,'Omega(',I3,')=',F20.12,6X,'1/(k*Omega(',I3,'))=',F20.12)
12      Continue
        End

        Subroutine Landen(k,KK)
        dimension a(40),b(40)
        Double Precision a,b,K,KK,PI
        PI=3.1415926535D0
        I=1
        a(1)=1.0D0
        b(1)=DSQRT(1-K*K)
7       a(I+1)=0.5*(a(I)+b(I))
        b(I+1)=DSQRT(a(I)*b(I))
        IF (ABS(A(I+1)-b(I+1)).LT.0.1D-10) Goto 200
        I=I+1
        go to 7
200     KK=PI/2/a(I+1)
        Return
        End

        Subroutine SSN(U,k,KK,KKp,Sn)
        Double Precision k,KK,KKp,U,Sn,Pi,Q,V,W
        Pi=3.1415926535D0
        Q=DExp(-Pi*KKp/KK)
        V=Pi/2.0*U/KK
        SN=0
        m=1
33      W=Q**(m-1)/(1.0-Q**(2*m-1))
        SN=SN+W*DSin((2*m-1)*V)
        m=m+1
        IF(W.GT.0.1D-20) Goto 33
        SN=SN*2*Pi*Dsqrt(Q)/k/KK
        Return
        End
