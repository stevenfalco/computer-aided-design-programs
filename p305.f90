        Program Sections
        Implicit Real (L)
        Dimension A(30),B(30)
        Complex S,Z,q1,q2,q3,q4
        Write(*,*) 'This is to realize The Darlington type-C &
                & Section,Brune Section, '
        Write(*,*) 'and The Darlington type-E Section'
        Write(*,*) 'To obtain Darlington type-D section, &
                & run Program P306'
        Write(*,*)
        Write(*,*) 'Input order of numerator polynomial M'
        Read(*,*) M
        Write(*,*) 'Coefficients of numerator polynomial:&
                & A(m),A(m-1),...,A(0)'
        Do 2 I=1,M+1
2       Read(*,*) A(I)
        Write(*,*) 'Input order of denominator polynomial N'
        Read(*,*) N
        Write(*,*) 'Coefficients of denominator polynomial: &
                & B(n),B(n-1),...,B(0)'
        Do 4 I=1,N+1
4       Read(*,*) B(I)
        Write(*,*) 's(x,y), complex number, &
                & at which index is defined'
        Read(*,*) s
        d0=real(s)
        W0=Aimag(s)
        Write(*,*)
        If(d0/=0.and.W0==0) Write(*,*) 'Z(s) is to be &
                & realized as Darlington type-C Section'
        If(d0==0.and.W0/=0) Write(*,*) 'Z(s) is to be &
                & realized as Brune Section'
        If (d0/=0.and.W0/=0) Write(*,*) 'Z(s) is to be &
                & realized as Darlington type-E section'
        Call pindex(A,M,B,N,Z,S,q1,q2,q3,q4)
        e1=real(q1)
        e2=real(q2)
        e3=real(q3)
        e4=real(q4)
        L1=1.0/(e1*e3)
        L2=e1/e3
        C=e3/CABS(s)**2
        Lm=1.0/e3
        If(d0/=0.and.W0==0) Lm=-1.0/e3
        Write(*,*) 'L1=',L1
        Write(*,*) 'L2=',L2
        Write(*,*) 'C =',C
        Write(*,*) 'M =',Lm
        IF(d0/=0.and.W0/=0) Write(*,*) 'Zeta=+-',2*d0/e3
        End
