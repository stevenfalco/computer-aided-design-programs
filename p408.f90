! Run with k1 = 0.0003820585:
! 
! $ ./p408
!  Input the Double Precision number k
! 0.0003820585
!  After           1   iterations
!  K(        0.0003820585)=        1.5707963841
! 
! This gives us K1 = 1.5707963841
! 
! #############################################################
! 
! Run with k' = 0.8660254038:
! 
! $ ./p408
!  Input the Double Precision number k
! 0.8660254038
!  a(  1)=        1.0000000000      b(  1)=        0.5000000000
!  a(  2)=        0.7500000000      b(  2)=        0.7071067812
!  a(  3)=        0.7285533906      b(  3)=        0.7282376575
!  After           4   iterations
!  K(        0.8660254038)=        2.1565156475
! 
! This gives us K' = 2.1565156475
! 
! #############################################################
! 
! Run with k1' = 0.9999999270:
! 
! $ ./p408
!  Input the Double Precision number k
! 0.9999999270
!  a(  1)=        1.0000000000      b(  1)=        0.0003820995
!  a(  2)=        0.5001910497      b(  2)=        0.0195473644
!  a(  3)=        0.2598692071      b(  3)=        0.0988808209
!  a(  4)=        0.1793750140      b(  4)=        0.1602999704
!  a(  5)=        0.1698374922      b(  5)=        0.1695694826
!  After           6   iterations
!  K(        0.9999999270)=        9.2561242878
! 
! This gives us K1' = 9.2561242878
! 
! #############################################################
! 
! Run with k = 0.5:
! 
! $ ./p408
!  Input the Double Precision number k
! .5
!  a(  1)=        1.0000000000      b(  1)=        0.8660254038
!  a(  2)=        0.9330127019      b(  2)=        0.9306048591
!  After           3   iterations
!  K(        0.5000000000)=        1.6857503548
! 
! This gives us K = 1.6857503548
! 
! #############################################################
! 
! n then equals (K * K1') / (K' * K1) = (1.6857503548 * 9.2561242878) / (2.1565156475 * 1.5707963841)
! 
! n = 15.6035148022 / 3.3874469813
! n = 4.6062757257
! 
! Therefore, we need a filter of degree 5, which is the next higher integer.
!
        Program P408
        dimension a(40),b(40)
        Double Precision a,b,K,KK,PI
        PI=3.1415926535D0
        Write(*,*) 'Input the Double Precision number k'
        Read(*,*) k
        I=1
        a(1)=1.0D0
        b(1)=DSQRT(1-K*K)
7       a(I+1)=0.5*(a(I)+b(I))
        b(I+1)=DSQRT(a(I)*b(I))
        IF(ABS(A(I+1)-b(I+1)).LT.0.1D-10) Goto 200
        Write(*,17) I,a(I),I,b(I)
17      Format (1X,'a(',I3,')=',F20.10,6X,'b(',I3,')=',F20.10)
        I=I+1
        go to 7
200     KK=PI/2/a(I+1)
        Write(*,*) 'After',I,'  iterations'
        Write(*,9) K,KK
9       Format (1X,'K(',F20.10,')=',F20.10)
        End
