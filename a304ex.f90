!       Long Division Example
        Dimension P(6),Q(5),S(2),R(4)
        Data P/0.0, 128.0, 0.0, 40.0, 0.0, 2.0/
        Data Q/9.0, 0.0, 10.0, 0.0, 1.0/
        Call OPDIV(P,6,Q,5,S,2,R,4)
        DO 10 I=1,2
10      WRITE (*,100) I,S(I)
        DO 20 I=1,4
20      WRITE (*,200) I,R(I)
100     Format(1X,'S(',I2,')=',D13.6)
200     Format(1X,'R(',I2,')=',D13.6)
        END
