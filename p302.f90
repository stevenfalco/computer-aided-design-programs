!       A Continued Fraction Expansion
        Program expand
        Dimension A(30),Q(30)
!       Expansion into continued fraction
        Write(*,*) 'Continual Fraction Expansion of PR Function'
        Write(*,*) 'Input the order of PR function'
        Read(*,*) N
        M=N+1
        K=M+1
        A(K)=0.0
        Write(*,*) 'Input the coefficients of PR function'
        Do 1 I=1,M
1       Read(*,*) A(I)
        Do 2 J=1,M
2       Write(*,*) 'A(',J,')=',A(J)
        Write(*,*)
        K=1
4       Q(K)=A(K)/A(K+1)
        Write(*,5) K,Q(K)
5       Format(3X,'Q(',I2,')=',G14.8)
        I=K+2
        IF(I>M) goto 8
7       Do 6 L=I,N,2
6       A(L)=A(L)-Q(K)*A(L+1)
        K=K+1
        goto 4
8       End
