! See the run on p84.
! 
! For equation 3.17, we turn the top eqn into the bottom as follows:
! 
! 1) Left most term stays the same
! 2) Next two terms are combined by multiplying the numerator and denominator
! by (s+j2)(s-j2).  We get:
! 
! .078125 * (s+j2) * (s-j2)   .078125 * (s+j2) * (s-j2) 
! ------------------------- + -------------------------
! (s+j2) * (s-j2) * (s+j2)     (s+j2) * (s-j2) * (s-j2)
! 
! Cross out one of the terms on both sides:
! 
! .078125 * (s-j2)   .078125 * (s+j2)
! ---------------- + ----------------
! (s+j2) * (s-j2)    (s+j2) * (s-j2)
! 
! Multiply them out:
! 
! .078125 s - j2 .078125   .078125 s + j2 .078125
! ---------------------- + ----------------------
! s^2 -j2s + j2s + 4         s^2 -j2s + j2s + 4
! 
! Simplify:
! 
! .078125 s + .078125 s    .156250 s
! --------------------- =  =========
!        s^2 + 4		  s^2 + 4
! 
! 3) Do the same for the last two terms and get:
! 
! 2 * 0.1367188 s    0.2734376 s
! --------------- =  -----------
!    s^2 + 16          s^2 + 16
! 
! As to the inductors, take the reciprocal of the numerator, so
! 1 / .0703125 = 14.22, 1 / .15625 = 6.4, 1 / 0.2734376 = 3.65714151967
! 
! For the capacitors, divide the numerator by the constant in the denominator,
! so .15625 / 4 = 0.0390625 and 0.2734376 / 16 = 0.01708985
! 
! To factor 3.16 (i.e. find the roots), we use program a206:
! 
! $ ./a206ex 
!  Input N, the order of polynomial
! 5
!  Input A(1),A(2),...,A(n)
! 0
! 20
! 0
! 64
! 0
!  A(           1 )=   0.0000000000000000     
!  A(           2 )=   20.000000000000000     
!  A(           3 )=   0.0000000000000000     
!  A(           4 )=   64.000000000000000     
!  A(           5 )=   0.0000000000000000     
!    X( 1)=   -0.7358774058E-15      Y( 1)=     2.000000000    
!    X( 2)=   -0.7358774058E-15      Y( 2)=    -2.000000000    
!    X( 3)=    0.2364627038E-15      Y( 3)=     4.000000000    
!    X( 4)=    0.2364627038E-15      Y( 4)=    -4.000000000    
!    X( 5)=    0.9988294041E-15      Y( 5)=     0.000000000
! 
! The real terms are small enough to be ignored, and we get the imaginary terms
! in +/- pairs as expected.  To check that result, the above becomes:
! 
! (s + 2j)(s - 2j)(s + 4j)(s - 4j)(s) or
! (s^2 - s2j + s2j + 4)(s^2 - s4j + s4j + 16)(s) or
! (s^2 + 4)(s^2 + 16)(s)
! 
!       Partial Fraction Expansion Example
        Program Partial
        Dimension A(25),B(25),P(25)
        Double Complex A,P
        Write(*,*) 'Partial Fraction Expansion of PR rational Function'
        Write(*,*) 'Denominator Degree='
        Read(*,*) N
        Write(*,*) 'Input Real numerator coefficients:'
        Do 10 I=1,N
        Write(*,12) N-I
        Read(*,*) B(I)
10      A(I)=Cmplx(B(I),0.0)
12      Format(2X,'Real Coefficients of S**',I2)
        Do 20 I=1,N
        Write(*,*) 'Root # in (*,*) ',I
        Read(*,*) P(I)
20      Write(*,*) P(I)
        Ep=1.0E-10
        Call PF(N,A,P,Ep)
        Write(*,*) 'The residues at each pole'
        Do 40 I=1,N
40      Write(*,*) "k",N+1-I,"=",A(I)
        End
