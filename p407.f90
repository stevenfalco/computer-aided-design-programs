        Program P407
        real k
        Write(*,*) '*************************************************************'
        Write(*,*) 'This program calculates elliptic integral of the first kind'
        Write(*,*) 'modules k by Simpson rule.'
        Write(*,*) 'k--- the modulus and 0<k<1'
        Write(*,*) '*************************************************************'
        PI=3.1415926535
        Write(*,*) 'Input k'
        Read(*,*) k
        Do 68 I=0,900
        Phi=0.1*I*PI/180
        Call simp2(0.0,Phi,0.1E-3,S,k)
        Write (*,8) k,Phi,S
8       Format (1X,'U(',F10.6,',',F10.6,')=',F20.10)
68      Continue
        End

        Subroutine simp2 (A,B,EPS,SUM,mok)
        Dimension F(2,30),FM(2,30),E(2,30),KRTN(30)
        real mok
        SUM=0.0
        T=1.0
        ABSA=1.0
        EST=1.0
        DA=B-A
        FA=FCT(A,mok)
        FB=FCT(B,mok)
        FP=4.0*FCT(0.5*(A+B),mok)
        X=A
        L=0
1       K=1
        L=L+1
        T=T*1.7
        DX=DA/3.0
        SX=DX/6.0
        FM1=4.0*FCT((X+0.5*DX),mok)
        F1=FCT((X+DX),mok)
        FM(1,L)=FP
        F(1,L)=FCT((X+2.0*DX),mok)
        FM(2,L)=4.0*FCT((X+2.5*DX),mok)
        F(2,L)=FB
        E1=SX*(FA+FM1+F1)
        E(1,L)=SX*(F1+FP+F(1,L))
        E(2,L)=SX*(F(1,L)+FM(2,L)+FB)
        S=E1+E(1,L)+E(2,L)
        ABSA=ABSA-ABS(EST)+ABS(E1)+ABS(E(1,L))+ABS(E(2,L))
        IF (EST.EQ.1.0) GO TO 5
        IF (T*ABS(EST-S).LE.EPS*ABSA) GO TO 2
        IF (L.LT.30) Go To 5
2       SUM=SUM+S
3       L=L-1
        T=T/1.7
        K=KRTN(L)
        DX=DX*3.0
        IF (K.EQ.3) IF(L-1) 6,6,3
        EST=E(K,L)
        FP=FM(K,L)
        FA=FB
        FB=F(K, L)
        K=K+1
        X=X+DA
4       DA=DX
        KRTN(L)=K
        GO TO 1
5       EST=E1
        FP=FM1
        FB=F1
        Go To 4
6       Return
        END

        Function FCT(X,k)
        real k
        FCT=1.0/(1.0-SIN(X)*SIN(X)*k*k)**0.5
        Return
        End
