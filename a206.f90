!       Solve an Nth-Order Nonlinear Equation
        Subroutine srpe(N,A,B,C,ES,ESD,KT,X,Y)
        Dimension A(N),B(N),C(N),X(N),Y(N)
        Double Precision A,B,C,X,Y,p,q,s,t,d,dp,dq,FX
        N1=N
        C(1)=1.0D0
        M1=1
        do 10 I=1,N
10      Y(I)=0.0D0
11      if (N1.LT.1.0D0) goto 15
        if (N1.EQ.1.0D0) goto 16
        if (N1.EQ.2.0D0) goto 17
        p=0.0D0
        q=0.0D0
        k=0
12      B(1)=A(1)-p
        B(2)=A(2)-p*B(1)-q
        do 1 I=3,N1
1       B(I)=A(I)-p*B(I-1)-q*B(I-2)
        s=B(N1-1)
        t=B(N1)+p*B(N1-1)
        if (ABS(s).GT.ES) goto 3
        if (ABS(t).LT.ES) goto 14
3       C(2)=B(1)-p*C(1)
        do 2 I=3,N1
2       C(I)=B(I-1)-p*C(I-1)-q*C(I-2)
        d=C(N1-1)**2-(C(I-1)-B(N1-1))*C(N1-2)
        dp=B(N1-1)*C(N1-1)-B(N1)*C(N1-2)
        dq=-B(N1-1)*(C(N1)-B(N1-1))+B(N1)*C(N1-1)
        if (ABS(d).GT.ESD) goto 13
        p=p+1.0D0
        q=q+1.0D0
        goto 12
13      k=k+1
        p=p+dp/d
        q=q+dq/d
        if (k.LT.KT) goto 12
        goto 15
14      X(M1)=-p/2.0D0
        X(M1+1)=-p/2.0D0
        FX=-p/2.0D0
        FX=FX**2-q
        if (ABS(FX).LT.1.0D-10) goto 4
        if (FX.LT.0.0D0) goto 5
        X(M1)=X(M1)+SQRT(FX)
        X(M1+1)=X(M1+1)-SQRT(FX)
        goto 4
5       Y(M1)=SQRT(ABS(FX))
        Y(M1+1)=-Y(M1)
4       M1=M1+2
        N1=N1-2
        do 6 I=1, N1
6       A(I)=B(I)
        goto 11
17      p=B(N1-1)
        q=B(N1)
        goto 14
16      X(M1)=-B(N1)
15      return
        end
