        Program p404
        Dimension T(0:20)
        Write(*,*) '********************************************************'
        Write(*,*) 'This program computes the coefficients of Chebyshev polynomial of form'
        Write(*,*) 'Cn(w)=T(N)*W**N+T(N-2)*W**(N-2)+...'
        Write(*,*) '********************************************************'
        Write(*,*) 'Input N'
        Read (*,*) N
        T(0)=1.0
        Do 10 I=0,Int(N/2)
        T(I)=1.0
        Do 20 J=1,N-I-1
20      T(I)=T(I)*J
        Do 30 J=1,I
30      T(I)=T(I)/J
        Do 40 J=1,N-2*I
40      T(I)=T(I)/J
        T(I)=2**(N-2*I)*(-1)**I*T(I)*N/2
        Write(*,50) N-2*I,T(I)
50      Format (1X,'T(',I3,')=',F16.6)
10      Continue
        End
