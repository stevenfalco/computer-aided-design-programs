#include <stdio.h>
#include <complex.h>
#include <math.h>

int
main()
{
	complex x = -1E30;

	complex fn = (cpow(x, 2) + 4.) / ((cpow(x, 2) + 3.) * (cpow(x, 2) + 7.));
	
	printf("%g, %g\n", creal(fn), cimag(fn));

	return 0;
}
