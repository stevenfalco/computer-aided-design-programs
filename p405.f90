        Program p405
        Write(*,*)'********************************************************'
        Write(*,*)' Determine the order of Chebyshev filter'
        Write(*,*)'********************************************************'
        Write(*,*)'Input Amax,Amin,Fp, and Fs'
        Read(*,*) Amax,Amin,Fp,Fs
        E=10**(0.1*Amax)-1.0
        F=10**(0.1*Amin)-1.0
        X=Sqrt(F/E)
        X=LOG(X+Sqrt(X*X-1.0))
        Y=Fs/Fp
        Y=LOG(Y+Sqrt(Y*Y-1.0))
        N=Int(X/Y)+1
        Write(*,10) N
10      Format(1X,'The order needed is',I3)
        End
