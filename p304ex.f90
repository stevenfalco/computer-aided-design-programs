        Program P304
        Dimension A(30),B(30)
        Complex S,Z,q1,q2,q3,q4
        Write(*,*) 'Input order of numerator polynomial M'
        Read(*,*) M
        Write(*,*) 'Coefficients of numerator polynomial: A(m),A(m-1),...,A(1),A(0)'
        Do 2 I=1,M+1
2       Read(*,*) A(I)
        Write(*,*) 'Input order of denominator polynomial N'
        Read(*,*) N
        Write(*,*) 'Coefficients of denominator polynomial: B(n),B(n-1),...,B(1),B(0)'
        Do 4 I=1,N+1
4       Read(*,*) B(I)
        Write(*,*) 's(x,y), complex number, at which index is defined'
        Read(*,*) s

        Call pindex(A,M,B,N,Z,S,q1,q2,q3,q4)
        Write(*,*) 'ql=',Real(q1)
        Write(*,*) 'q2=',Real(q2)
        Write(*,*) 'q3=',Real(q3)
        Write(*,*) 'q4=',Real(q4)

        End Program
