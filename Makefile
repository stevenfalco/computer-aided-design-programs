TARGETS = 		\
	a101ex		\
	a206ex		\
	a303		\
	a304ex		\
	p301ex		\
	p302		\
	p303		\
	p304ex		\
	p305		\
	p306		\
	p401		\
	p402		\
	p403		\
	p404		\
	p405		\
	p406		\
	p407		\
	p408		\
	p409		\
	p410		\
	#

OBJDIR = obj
EXEDIR = exe

DIRS = $(OBJDIR) $(EXEDIR)

OBJ = $(patsubst %,$(OBJDIR)/%.o,$(TARGETS))
EXE = $(patsubst %,$(EXEDIR)/%,$(TARGETS))

FFLAGS = -std=legacy

all: $(DIRS) $(OBJ) $(EXE)

$(EXEDIR)/a101ex: $(OBJDIR)/a101ex.o $(OBJDIR)/a101.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/a206ex: $(OBJDIR)/a206ex.o $(OBJDIR)/a206.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/a303: $(OBJDIR)/a303.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/a304ex: $(OBJDIR)/a304ex.o $(OBJDIR)/a304.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p301ex: $(OBJDIR)/p301ex.o $(OBJDIR)/p301.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p302: $(OBJDIR)/p302.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p303: $(OBJDIR)/p303.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p304ex: $(OBJDIR)/p304ex.o $(OBJDIR)/p304.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p305: $(OBJDIR)/p305.o $(OBJDIR)/p304.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p306: $(OBJDIR)/p306.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p401: $(OBJDIR)/p401.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p402: $(OBJDIR)/p402.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p403: $(OBJDIR)/p403.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p404: $(OBJDIR)/p404.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p405: $(OBJDIR)/p405.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p406: $(OBJDIR)/p406.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p407: $(OBJDIR)/p407.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p408: $(OBJDIR)/p408.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p409: $(OBJDIR)/p409.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(EXEDIR)/p410: $(OBJDIR)/p410.o
	$(announce)
	gfortran $(FFLAGS) -o $@ $^

$(OBJDIR)/%.o: %.f90
	$(announce)
	gfortran $(FFLAGS) -c -o $@ $^

$(DIRS):
	$(announce)
	[ -d $(@) ] || mkdir -p $(@)

clean:
	$(announce)
	rm -fr $(DIRS)

announce = @echo; \
	   if [ -f "$@" ]; \
	   then echo "* $@: $?"; \
	   else echo "* $@:"; \
	   fi; \
	   echo
