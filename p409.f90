        Program P409
        Real*8 k1,KK1,k2,KK2,k3,KK3,k4,KK4
        Write(*,*) 'Input Amax, Amin, Fp and Fs'
        Read(*,*) Amax,Amin,Fp,Fs
        Emax=10**(0.1*Amax)-1.0
        Emin=10**(0.1*Amin)-1.0
        k1=Sqrt(Emax/Emin)
        Call Eintg(k1,KK1)
        Write(*,50) k1,KK1
50      Format (1X,'k1 =',F16.10,6X,'K( k1)=',F16.10)

        k2=Sqrt(1.0-k1*k1)
        Call Eintg(k2,KK2)
        Write(*,100) k2,KK2
100     Format (1X, 4Hk1'=,F16.10,6X,7HK(k1')=,F16.10)

        k3=Fp/Fs
        Call Eintg(k3,KK3)
        Write(*,200) k3,KK3
200     Format (1X,'k  =',F16.10,6X,'K( k )=',F16.10)

        k4=Sqrt(1.0-k3*k3)
        Call Eintg(k4, KK4)
        Write(*,300) k4,KK4
300     Format (1X,4Hk' =,F16.10,6X,7HK( k')=,F16.10)

        N=Int(KK2/KK1*KK3/KK4)+1
        Write(*,480) N
480     Format (6X,'N=',I3)
        End

        Subroutine Eintg(K, KK)
        Parameter (M=30)
        Real*8 A(0:M),O(0:M),K,KK,Pi,P,X,Y,E
        Pi=3.1415926535D0
        A(0)=ATAN2(K,SQRT(1-K*k))
        O(0)=Pi/2.0

        P=1.0
        I=0
100     X=2.0/(1.0+SIN(A(I)))-1.0
        Y=SIN(A(I))*SIN(O(I))
        A(I+1)=ATAN2(SQRT(1-X*X),X)
        O(I+1)=0.5*(O(I)+ATAN2(Y,SQRT(1-Y*Y)))
        E=1-A(I+1)*2/Pi
        I=I+1
        IF (E.GT.0.1E-7) Goto 100
        Do 10 J=1,I
        P=P*(1+COS(A(J)))
10      Continue
        X=Pi/4+O(I)/2
        KK=LOG(ABS(SIN(X)/COS(X)))*P
        Return
        End
