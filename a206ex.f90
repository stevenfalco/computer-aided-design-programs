!       Solve an Nth-Order Nonlinear Equation
        Program findroot
        Dimension A(20),B(20),C(20),X(20),Y(20)
        Double precision A,B,C,X,Y
        Write(*,*) 'Input N, the order of polynomial'
        Read(*,*) N
        Write(*,*) 'Input A(1),A(2),...,A(n)'
        do 10 I=1,N
10      Read(*,*) A(I)
        do 15 I=1,N
15      Write(*,*) 'A(',I,')=',A(I)
        ES=1.0D-12
        ESD=1.0D-20
        KT=1000
        Call srpe(N,A,B,C,ES,ESD,KT,X,Y)
        do 20 I=1,N
        Write (*,22) I,X(I),I,Y(I)
22      Format (3X,'X(',I2,')=',G20.10,6X,'Y(',I2,')=',G20.10)
20      Continue
        End
