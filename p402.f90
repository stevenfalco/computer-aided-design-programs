        Program p402
        Dimension A(0:20),B(0:20)
        Double Precision A,Alpha
        Write(*,*)'******************************************************'
        Write(*,*)'Program P402 yields reflection coefficient of Butterworth filter'
        Write(*,*)'N---is the order of Butterworth filter'
        Write(*,*)'H0---dc gain'
        Write(*,*)'Numerator polynomial B(N)*S**N+B(N-1)*s**(N-1)+...+B(1)*S+B(0)'
        Write(*,*)'Denominator polynomial A(N)*S**N+A(N-1)*s**(N-1)+...+A(1)*S+A(0)'
        Write(*,*)'******************************************************'
        Write(*,*)'Input N,H0'
        Read(*,*) N,H0
        Do 10 k=0,N
        Call Co(N,K,A)
10      Continue

        IF(H0.EQ.1.0) Then
        Alpha=0.0
        Do 20 I=0,N-1
20      B(I)=0.0
        B(N)=1.0
        Else
        Alpha=(1.0D0-H0)**(1.0D0/(2*N))
        Do 30 k=0,N
30      B(k)=A(k)*Alpha**k
        Endif
        Do 40 I=0,N
40      Write(*,50) N-I,A(I),N-I,B(I)
50      Format(3X,'a(',I2,')=',F14.8,6X,'b(',I2,')=',F14.8)
        End

        Subroutine Co(N,k,A)
        Dimension A(0:N)
        Double Precision A, Pi
        Pi=3.1415926535D0
        A(0)=1.0D0
        Do 100 I=1,k
100     A(I)=A(I-1) *DCOS((I-1)*Pi/(2*N))/DSIN(I*Pi/(2*N))
        End
