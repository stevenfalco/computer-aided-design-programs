        PROGRAM determinant
        Dimension A(4,4)
        DATA A/2,-3,1,2,-3,5,-2,-1,1,-2,7,-5,2,-1,-5,16/
        Call DETERM (A,4,0.1E-10,DET)
        Write (*,111) DET
111     Format (1X,"DET=",F20.10)
        End
