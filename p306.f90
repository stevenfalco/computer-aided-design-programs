        Program DSection
        Implicit Real(L)
        Dimension A(30),B(30)
        Complex S,Z,Pn,Pd,Ans1,Ans2,dZ
        Write(*,*) 'This is to realize The Darlington type-D Section!'
        Write(*,*)
        Write(*,*) 'Input order of numerator polynomial M'
        Read(*,*) M
        Write(*,*) 'Coefficients of numerator polynomial: &
                & A(m),A(m-1),...,A(0)'
        Do 2 I=1,M+1
2       Read(*,*) A(I)
        Write(*,*) 'Input order of denominator polynomial N'
        Read(*,*) N
        Write(*,*) 'Coefficients of denominator polynomial: &
                & B(n),B(n-1),...,B(0)'
        Do 4 I=1,N+1
4       Read(*,*) B(I)
        Write(*,*) 's(x,y), complex number, at which index is defined'
        Read(*,*) s
        Pn=A(1)
        Do 10 I=2,M+1
        Pn=Pn*S+A(I)
10      Continue
        Pd=B(1)
        Do 20 I=2,N+1
        Pd=Pd*S+B(I)
20      Continue
        d=Real(S)
        w=Aimag(S)
        Z=Pn/Pd
        R=Real(Z)
        X=Aimag(Z)

        Call Plydif(A,M+1,s,Ans1)
        Call Plydif(B,N+1,s,Ans2)
        dZ=(Ans1*Pd-Pn*Ans2)/Pd**2
        U=Real(dZ)
        V=Aimag(dZ)

        e1=x*d**3-U*d*w*(d**2+w**2)+r*w**3
        e2=-x*d**3+U*d*w*(d**2-w**2)-2*V*d*d*w*w+r*w**3
        e3=V*(d*d+w*w)+3*w*r-3*d*x
        e4=-V*(d*d-w*w)-2*d*w*U+w*r+d*x
        h1=e2*(x*d*(3*w*w-d*d)+r*w*(3*d*d-w*w))
        h1=h1+e1*(d*d+w*w)*(r*w-x*d)
        h2=1.0/(e2*e3-e1*e4)
        g1=2*(w*w-d*d)+4*h1*h2
        g2=1+4*h2*(e1*(x*d+r*w)+e2*(x*d-r*w))
        g5=8*d*w*(d*d+w*w)*e2*h2
        g6=8*d*w*e1*h2
        Lm1=1.0/g6
        L1=g2/g6
        L2=Lm1**2/L1
        Lm2=g5*g5-2*(w*w-d*d)*g5*g6+CABS(s)**4*g6**2
        Lm2=-Lm2/(g5**2*g6)
        L3=(g1*g5*g6-g2*g5**2-CABS(s)**4*g6**2)/(g5**2*g6)
        Write(*,*) 'M1=',Lm1
        Write(*,*) 'L1=',L1
        Write(*,*) 'L2=',L2
        Write(*,*) 'M2=',Lm2
        Write(*,*) 'L3=',L3
        Write(*,*) 'L4=',Lm2**2/L3
        Write(*,*) 'C1=',g6/(g5*L3)
        Write(*,*) 'C2=',g5/CABS(s)**4
        End

        Subroutine Plydif(A,N,X,Ans)
        Complex X,Ans
        Dimension A(N)
        Pwr=N-1
        Ans=A(1)*Pwr
        IF(N.LE.1) Return
        Do 10 I=2,N-1
        Pwr=Pwr-1
        Ans=Ans*X+Pwr*A(I)
10      Continue
        Return
        End
