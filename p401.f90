        Program P401
        Write(*,*)'*******************************************'
        Write(*,*)'Program P401 is to determine order n and normalized frequency F0'
        Write(*,*)'for the Butterworth filter, whose attenuation'
        Write(*,*) 'A(w)<=Amax in passband, A(w)>=Amin in stop band '
        Write(*,*)'   Amax---is the maximum pass band attenuation in dB'
        Write(*,*)'   Amin---is the minimum stop band attenuation in dB'
        Write(*,*)'   Fp  ---is the pass band frequency in Hz'
        Write(*,*)'   Fs  ---is the stop band frequency in Hz'
        Write(*,*)'*******************************************'
        Write(*,*)
        Write(*,*) 'Input Amax, Amin, Fp and Fs'
        Read(*,*) Amax,Amin,Fp,Fs
        A=10**(0.1*Amax)-1.0
        B=10**(0.1*Amin)-1.0
        C=Fs/Fp
        N=INT(LOG(B/A)/(2*LOG(C)))+1
        Write(*,*) 'N=',N
        F1=Fp/(10**(0.1*Amax)-1.0)**(1.0/(2*N))
        F2=Fs/(10**(0.1*Amin)-1.0)**(1.0/(2*N))
        Write(*,*) 'FO should be larger than',F1,'Hz'
        Write(*,*) 'FO should be less than',F2,'Hz'
        F0=Sqrt(F1*F2)
        Write(*,*) 'We choose F0 to be',F0,'Hz'
        End
