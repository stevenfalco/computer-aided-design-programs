       Program Partial
       Dimension A(25),B(25),P(25)
       Complex A,P
       Write(*,*) 'Partial Fraction Expansion of PR rational Function'
       Write(*,*) 'The order of Denominator Polynomial N=?'
       Read(*,*) N
       Write(*,*) 'Input Real numerator coefficients:'
       Do 10 I=1,N
       Write(*,12) N-I
       Read (*,*) B(I)
10     A(I)=Cmplx(B(I),0.0)
12     Format (2X, 'Real Coefficient of S**',I2)
       Write(*,*) 'Input Denominator Roots in Order of ascending magnitudes'
       Do 20 I=1,N
       Write(*,*) 'Root #',I,'in (*,*) form'
       Read(*,*) P(I)
20     Write(*,*) P(I)
       Ep=1.0E-10
       Call PF(N,A,P,Ep)
       Write(*,*) '_______________________________________________________'
       Do 40 I=1,N
40     Write(*,*) "k",N+1-I,"=",A(I)
       End

       Subroutine PF(N,A,P,Ep)
       Complex A(N),P(N),Y
       Do 4 I=1,N
       I1=N-I+1
       IF(I1.EQ.1) Goto 3
       Do 2 J=2,I1
       A(J)=A(J)+P(I)*A(J-1)
2      Continue
3      Do 5 J=1,I
       J1=N-J+1
       IF(J.EQ.1) Goto 7
       IF(CABS(P(J)-P(J-1)).LE.Ep) Goto 6
       A(I1)=A(I1)-A(J1+1)
7      IF(CABS(P(J)-P(I)).LE.Ep) Goto 4
       Y=P(J)-P(I)
       A(J1)=A(J1)/Y
       Goto 5
6      A(J1)=(A(J1)-A(J1+1))/Y
5      Continue
       Write(*,*) P(I),A(I)
4      Continue
       Return
       End
