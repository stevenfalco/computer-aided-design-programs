        Program p403
        !'To calculate the element values of the Butterworth Filter
	Implicit Real (L)
        Dimension C(20),L(21)
        Write(*,*) 'Input N,R1,R2,W0'
        Read(*,*) N,R1,R2,W0
        Pi=3.1415926
        Gamma=Pi/(2.0*N)
        H0=4*R1*R2/(R1+R2)**2
        Alpha=(1.0-H0)**(1.0/(2.0*N))
        IF(R1.GT.R2) Goto 200
        L(1)=2*R1*SIN(Gamma)/((1.0-Alpha)*W0)
        Write(*,28) L(1)
28      Format (28X,'L( 1)=',E14.9)
        Do 10 I=1,N/2
        C(2*I)=4*Sin((4*I-3)*Gamma)*Sin((4*I-1)*Gamma)
        C(2*I)=C(2*I)/(W0*W0*(1.0-2*Alpha*Cos((4*I-2)*Gamma)+Alpha**2))
        C(2*I)=C(2*I)/L(2*I-1)
        L(2*I+1)=4*SIN((4*I-1)*Gamma)*SIN((4*I+1)*Gamma)
        L(2*I+1)=L(2*I+1)/(W0**2*(1-2*Alpha*COS(4*I*Gamma)+Alpha**2))
        L(2*I+1)=L(2*I+1)/C(2*I)
        IF (2*I+1.GT.N) goto 99
        Write(*,38) 2*I,C(2*I),2*I+1,L(2*I+1)
38      Format (2X,'C(',I2,')=',E14.9,6X,'L(',I2,')=',E14.9)
10      Continue
        Goto 100
99      Write(*,48) 2*I,C(2*I)
48      Format (2X,'C(',I2,')=',E14.9)
        goto 100

200     C(1)=2*SIN(Gamma)/(R1*(1.0-Alpha)*W0)
        Write(*,128) C(1)
128     Format (28X,'C( 1)=',E14.9)
        Do 110 I=1,N/2
        L(2*I)=4*Sin((4*I-3)*Gamma)*Sin((4*I-1)*Gamma)
        L(2*I)=L(2*I)/(W0*W0*(1.0-2*Alpha*Cos((4*I-2)*Gamma)+Alpha**2))
        L(2*I)=L(2*I)/C(2*I-1)
        C(2*I+1)=4*SIN((4*I-1)*Gamma)*SIN((4*I+1)*Gamma)
        C(2*I+1)=C(2*I+1)/(W0**2*(1-2*Alpha*COS(4*I*Gamma)+Alpha**2))
        C(2*I+1)=C(2*I+1)/L(2*I)
        IF (2*I+1.GT.N) goto 199
        Write(*,138) 2*I,L(2*I),2*I+1,C(2*I+1)
138     Format (2X,'L(',I2,')=',E14.9, 6X,'C(',I2,')=',E14.9)
110     Continue
        Goto 100
199     Write(*,148) 2*I,L(2*I)
148     Format (2X,'L(',I2,')=',E14.9)
100     End
