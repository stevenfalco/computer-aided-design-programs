        Subroutine pindex(A,M,B,N,Z,S,q1,q2,q3,q4)
        Dimension A(M+1),B(N+1)
        Complex S,Z,Pn,Pd,X,Ans1,Ans2
        Complex q1,q2,q3,q4
        Ep=0.1e-04
        Pn=A(1)
        Do 10 I=2,M+1
        Pn=Pn*S+A(I)
10      Continue

        Pd=B(1)
        Do 20 I=2,N+1
        Pd=Pd*S+B(I)
20      Continue

        d0=Real(S)
        w0=Aimag(S)
        Z=Pn/Pd
        Write(*,*) 'Z=',Z
        R0=Real(Z)
        X0=Aimag(Z)

        If (ABS (w0)<=Ep) Go to 100
        If (ABS (d0)<=Ep.And.ABS(R0)<=Ep) Go to 200
        q1=(R0/d0-X0/w0) / (R0/d0+X0/w0)
        q2=2*CABS(Z)**2 / (R0/d0+X0/w0)
        q3=2.0/ (R0/d0-X0/w0)
        q4=1.0/q1
        Write(*,*) 'CABS(Z)=',CABS(Z)
        Go to 300

100     X=Cmplx(d0,0.0)
        Call Plydif(A,M+1,X,Ans1)
        Call Plydif(B,N+1,X,Ans2)
        dZ=(Ans1*Pd-Pn*Ans2)/Pd**2
        Write(*,*) 'dZ=',dZ
        q1=(R0/d0-dZ) / (R0/d0+dZ)
        q2=2*CABS(Z)**2 / (R0/d0+dZ)
        q3=2.0/ (R0/d0-dZ)
        q4=1.0/q1
        Write(*,*) 'CABS(Z)=',CABS(Z)
        Go to 300

200     X=Cmplx(0.0,W0)
        Call Plydif(A,M+1,X,Ans1)
        Call Plydif(B,N+1,X,Ans2)
        dX=(Ans1*Pd-Pn*Ans2)/Pd**2
        Write(*,*) 'dX=',dX

        q1=(dX-X0/w0) / (dX+X0/w0)
        q2=2*CABS(Z)**2 / (dX+X0/w0)
        q3=2.0/(dX-X0/w0)
        q4=1.0/q1
        Write(*,*) 'CABS(Z)=',CABS(Z)
300     Return
        End

        Subroutine Plydif(A,N,X,Ans)
        Complex X,Ans
        Dimension A(N)
        Pwr=N-1
        Ans=A(1)*Pwr
        IF(N.LE.1) Return
        Do 10 I=2,N-1
        Pwr=Pwr-1
        Ans=Ans*X+Pwr*A(I)
10      Continue
        Return
        End
