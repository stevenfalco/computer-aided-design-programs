        SUBROUTINE DETERM (A,N,EPS,DET)
        DIMENSION A(N,N)
        SQN=1.0
        PR=1.0D0
        N1=N-1
        DO 14 K=1,N1
        P=0.0D0
        DO 11 I=K,N
        DO 11 J=K,N
        IF(ABS(A(I,J)).LE.P) GOTO 11
        P=ABS(A(I,J))
        I0=I
        J0=J
11      CONTINUE
        IF(P.LE.EPS) GOTO 20
        IF(I0.EQ.K) GOTO 15
        SQN=-SQN
        DO 12 J=K,N
        C=A(I0,J)
        A(I0,J)=A(K,J)
        A(K,J)=C
12      CONTINUE
15      IF(J0.EQ.K) GOTO 16
        SQN=-SQN
        DO 13 I=K,N
        C=A(I,J0)
        A(I,J0)=A(I,K)
        A(I,K)=C
13      CONTINUE
16      PR=PR*A(K,K)
        K1=K+1
        DO 14 I=K1,N
        Q=A(I,K)/A(K,K)
        DO 14 J=K1,N
        A(I,J)=A(I,J)-Q*A(K,J)
14      CONTINUE
        DET=SQN*PR*A(N,N)
        GOTO 21
20      DET=0.0D0
21      RETURN
        END
