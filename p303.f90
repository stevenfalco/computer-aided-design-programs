!       Realization of a mid-shunt low-pass ladder network
        Program Midshu
        Dimension A(0:20),B(0:20),W(10),C(10,10),L(10,10),PP(10,10),BB(10,10),DD(10,10),X(10)
        Double Precision A,B,C,L,X,W,P,Q,Q1,DP,PP,DD,BB,CC,LL
        Write(*,*) 'Input order of numerator polynomial, n is odd'
        Read(*,*) N
        Write(*,*) 'Input coefficients of numerator polynomial A(n),A(n-1),...,A(1),A(0)'
        Do 200 I=N,0,-1
        Read(*,*) A(I)
200     Write(*,202) I,A(I)
202     Format(2X,'A(',I2,')=',F20.12)
        Write(*,*) 'Input coefficients of denominator polynomial B(n-1),B(n-3),...,B(2),B(0)'
        Do 210 J=N-1,0,-2
        Read(*,*) B(J)
210     Write(*,212) J,B(J)
212     Format(2X,'B(',I2,')=',F20.12)
        Write(*,*) 'Input attenuation poles: W(1),W(2),...,W(n/2)'
        Do 220 K=1,N/2
        Read(*,*) W(K)
220     Write(*,225) K,W(K)
225     Format(2X,'W(',I2,')=',F20.12)
        Do 10 K=1,N/2
        X(K)=1.0/(W(K)*W(K))
        Q=B(0)
        Q1=A(0)
        P=A(1)
        DQ=N/2*B(0)
        DP=N/2*A(1)
        Do 3 I=1,N/2
        Q=-X(K)*Q+B(2*I)
        Q1=-X(K)*Q1+A(2*I)
3       P=-X(K)*P+A(2*I+1)
        Do 4 I=1,N/2-1
        DQ=X(K)*DQ+(-1)**I*(N/2-I)*B(2*I)
4       DP=X(K)*DP+(-1)**I*(N/2-I)*A(2*I+1)
        L(K,1)=-1.0/((DP*Q-DQ*P)/(Q*Q))
        C(K,1)=P/Q
10      Continue

        Do 11 I=2,N/2
        Do 12 J=I,N/2
        PP(J,I)=X(J)-X(I-1)
        BB(J,I)=C(J,I-1)-C(I-1,I-1)
        DD(J,I)=PP(J,I)+BB(J,I)*L(I-1,I-1)
        C(J,I)=PP(J,I)*BB(J,I)/DD(J,I)
        L(J,I)=PP(J,I)**2/L(J,I-1)-BB(J,I)**2*L(I-1,I-1)
        L(J,I)=DD(J,I)**2/L(J,I)
12      Continue
11      Continue

        Do 40 I=1,N/2
        CC=C(I,I)
        LL=L(I,I)
        Ge=X(I)/LL
        Write(*,38) I,CC,I,LL,I,Ge
38      Format(1X,'C(',I2,')='F10.7,2X,'L(',I2,')=',F10.7,2X,&
        &  'Gamma(',I2,')=',F10.7)
40      Continue

        If(N.EQ.2*(N/2)) goto 140
        C(N/2+1,1)=P/Q1
        Write(*,138) N/2+1,C(N/2+1,1)
138     Format(1X,'C(',I2,')=',F10.6)
140     End
